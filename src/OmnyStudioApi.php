<?php

namespace Drupal\omnystudio;

use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Class OmnyStudioApi.
 */
class OmnyStudioApi implements OmnyStudioApiInterface {

  /**
   * API endpoint base.
   */
  const API_BASE = 'https://api.omnystudio.com';

  /**
   * Response timeout in seconds.
   *
   * @var CONSTANT
   */
  const TIMEOUT = 300;

  /**
   * Connection timeout in seconds.
   *
   * @var CONSTANT
   */
  const CONNECT_TIMEOUT = 60;

  /**
   * Drupal\Core\Http\ClientFactory definition.
   *
   * @var \Drupal\Core\Http\ClientFactory
   */
  protected $clientFactory;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The API key.
   *
   * @var VARCHAR
   */
  protected $apiKey;

  /**
   * The page size.
   *
   * @var INT
   */
  protected $pageSize;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  private $loggerChannelFactory;

  /**
   * Constructs a new OmnyStudioApi object.
   */
  public function __construct(ClientFactory $clientFactory, ConfigFactoryInterface $config_factory, Connection $database, LoggerChannelFactoryInterface $loggerChannelFactory) {
    $this->clientFactory = $clientFactory;
    $this->config = $config_factory->get('omnystudio.settings');
    $this->apiKey = $this->config->get('api_key');
    $this->pageSize = $this->config->get('page_size');
    $this->database = $database;
    $this->loggerChannelFactory = $loggerChannelFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('http_client_factory'),
      $container->get('database'),
      $container->get('logger.factory')
    );
  }

  /**
   * Get an HTTP client for making a request.
   */
  protected function getClient() {
    return $this->clientFactory->fromOptions([
      'base_uri' => self::API_BASE,
      'headers' => ['Authorization' => 'OmnyToken ' . $this->apiKey],
      'timeout' => self::TIMEOUT,
      // Connection timeout in seconds.
      'connect_timeout' => self::CONNECT_TIMEOUT,
    ]);
  }

  /**
   * Test if the API is available by testing for a 400 or 200 response.
   *
   * @return bool
   *   Returns if the api response can be returned.
   */
  public function apiAvailable() {
    $uri = '/v0/test/ping';
    // GuzzleHTTP might throw an exception if we get a 400 response,
    // which still verifies that the API is up.
    try {
      $response = $this->getClient()->get(
        $uri,
        [
          'headers'        => ['Authorization' => 'OmnyToken ' . $this->apiKey],
          // Response timeout in seconds.
          'timeout' => self::TIMEOUT,
          // Connection timeout in seconds.
          'connect_timeout' => self::CONNECT_TIMEOUT,
        ]
      );

      return $response->getStatusCode() === 200;
    }
    catch (RequestException $e) {
      return $e->getResponse()->getStatusCode() === 401;
    }
  }

  /**
   * Test that the set API key and secret are valid.
   *
   * @return bool
   *   Returns if the credentials are valid.
   */
  public function validCredentials($api_key) {
    $uri = '/v0/test/ping';
    try {
      $response = $this->getClient()->get(
        $uri,
        [
          'headers'        => ['Authorization' => 'OmnyToken ' . $api_key],
          // Response timeout in seconds.
          'timeout' => self::TIMEOUT,
          // Connection timeout in seconds.
          'connect_timeout' => self::CONNECT_TIMEOUT,
        ]
      );
      if ($response->getStatusCode() == 200) {
        if (json_decode($response->getBody()) === 'PONG') {
          return TRUE;
        }
      }
    }
    catch (RequestException $e) {
      // Just fall through to returning false.
    }

    return FALSE;
  }

  /**
   * Get body of the requested endpoint.
   */
  private function getApi($uri, $query) {
    try {
      $response = $this->getClient()->get($uri, ['query' => $query]);
      if ($response->getStatusCode() == 200) {
        return $response->getBody();
      }
      elseif ($response->getStatusCode() == 429) {
        $x_ratelimit_credits = $response->getHeaders()['X-Ratelimit-Credits']['0'];
        $x_ratelimit_reset = $response->getHeaders()['X-Ratelimit-Reset']['0'];
        $this->loggerChannelFactory->get('omnystudio')->error('[%x_ratelimit_credits -  %x_ratelimit_reset] Api exited with %status_response.', [
          '%status_response' => $response->getStatusCode(),
          '%x_ratelimit_credits' => $x_ratelimit_credits,
          '%x_ratelimit_reset' => $x_ratelimit_reset,
        ]);
      }
    }
    catch (\Exception $ex) {
      // Log the exception to watchdog.
      $this->loggerChannelFactory->get('omnystudio')->error($ex->getMessage());
    }
  }

  /**
   * Post body of the requested endpoint.
   */
  private function postApi($uri, $params) {
    try {
      $response = $this->getClient()->post($uri, $params);
      if ($response->getStatusCode() == 200 || $response->getStatusCode() == 201) {
        $this->loggerChannelFactory->get('omnystudio')->notice('Post request was made for this uri [%uri].', [
          '%uri' => $uri,
        ]);
        return $response->getBody();
      }
      elseif ($response->getStatusCode() == 429) {
        $x_ratelimit_credits = $response->getHeaders()['X-Ratelimit-Credits']['0'];
        $x_ratelimit_reset = $response->getHeaders()['X-Ratelimit-Reset']['0'];
        $this->loggerChannelFactory->get('omnystudio')->error('[%x_ratelimit_credits -  %x_ratelimit_reset] Api exited with %status_response.', [
          '%status_response' => $response->getStatusCode(),
          '%x_ratelimit_credits' => $x_ratelimit_credits,
          '%x_ratelimit_reset' => $x_ratelimit_reset,
        ]);
      }
    }
    catch (\Exception $ex) {
      // Log the exception to watchdog.
      $this->loggerChannelFactory->get('omnystudio')->error($ex->getMessage());
    }
  }

  /**
   * Put body of the requested endpoint.
   */
  private function putApi($uri, $params) {
    try {
      $response = $this->getClient()->put($uri, $params);
      if ($response->getStatusCode() == 200 || $response->getStatusCode() == 303) {
        return $response->getBody();
      }
      elseif ($response->getStatusCode() == 429) {
        $x_ratelimit_credits = $response->getHeaders()['X-Ratelimit-Credits']['0'];
        $x_ratelimit_reset = $response->getHeaders()['X-Ratelimit-Reset']['0'];
        $this->loggerChannelFactory->get('omnystudio')->error('[%x_ratelimit_credits -  %x_ratelimit_reset] Api exited with %status_response.', [
          '%status_response' => $response->getStatusCode(),
          '%x_ratelimit_credits' => $x_ratelimit_credits,
          '%x_ratelimit_reset' => $x_ratelimit_reset,
        ]);
      }
    }
    catch (\Exception $ex) {
      // Log the exception to watchdog.
      $this->loggerChannelFactory->get('omnystudio')->error($ex->getMessage());
    }
  }

  // Network Section.

  /**
   * List all networks.
   */
  public function getNetworks() {
    $uri = '/v0/networks';
    $params = [];
    return $this->getApi($uri, $params);
  }

  /**
   * List all networks.
   */
  public function getformattedNetworks() {
    $uri = '/v0/networks';
    $params = [];
    $networks = json_decode($this->getApi($uri, $params));
    if ($networks) {
      $func = function ($network) {
        return $network->Name;
      };
      return array_map($func, $networks->Networks);
    }
  }

  // Program Section.

  /**
   * Provides a list of modified and deleted Programs.
   *
   * Since the last cursor position & associated with the provided network name.
   */
  public function getProgramChanges($cursor = '', $network = '') {
    $uri = '/v0/programs/changes';
    $params = [
      'network' => $network,
      'pageSize' => $this->pageSize,
      'cursor' => $cursor,
    ];
    return $this->getApi($uri, $params);
  }

  /**
   * List all programs.
   */
  public function getPrograms() {
    $uri = '/v0/programs';
    $params = [];
    return $this->getApi($uri, $params);
  }

  /**
   * List programs associated with the provided network name.
   */
  public function getProgramsByNetworkName($networkName) {
    $uri = '/v0/networks/programs-by-network-name';
    $params = ['networkName' => $networkName];
    return $this->getApi($uri, $params);
  }

  /**
   * Retrieve a program with the provided Id.
   */
  public function getProgramById($id) {
    $uri = '/v0/programs/' . $id;
    $params = [];
    return $this->getApi($uri, $params);
  }

  // Playlist Section.

  /**
   * Provides a list of modified and deleted Playlists.
   *
   * Since the last cursor position & associated with the provided network name.
   */
  public function getPlaylistsChanges($cursor = '', $network = '') {
    $uri = '/v0/playlists/changes';
    $params = [
      'network' => $network,
      'pageSize' => $this->pageSize,
      'cursor' => $cursor,
    ];
    return $this->getApi($uri, $params);
  }

  /**
   * List all Playlists for a given Program.
   */
  public function getPlaylistsByProgramId($programId) {
    $uri = '/v0/programs/' . $programId . '/playlists';
    $params = [];
    return $this->getApi($uri, $params);
  }

  /**
   * Retrieve a playlist with the provided Id.
   */
  public function getPlaylistById($playlistId) {
    $uri = '/v0/playlists/' . $playlistId;
    $params = [];
    return $this->getApi($uri, $params);
  }

  /**
   * List Playlists whose Program's are associated with the provided network.
   */
  public function getPlaylistsByNetworkName($networkName) {
    $uri = '/v0/networks/playlists-by-network-name';
    $params = ['networkName' => $networkName];
    return $this->getApi($uri, $params);
  }

  // Clips Section.

  /**
   * Retrieve a list of Clips from a given Playlist.
   */
  public function getClipsByPlaylist($playlistId, $cursor = '') {
    $uri = '/v0/playlists/' . $playlistId . '/clips';
    $params = [
      'cursor' => $cursor,
    ];
    return $this->getApi($uri, $params);
  }

  /**
   * Provides a list of modified and deleted Clips.
   *
   * Since the last cursor position & associated with the provided network name.
   */
  public function getClipsChanges($cursor = '', $network = '') {
    $uri = '/v0/clips/changes';
    $params = [
      'network' => $network,
      'pageSize' => $this->pageSize,
      'cursor' => $cursor,
    ];
    return $this->getApi($uri, $params);
  }

  /**
   * Retrieve a list of Clips for a given Program.
   */
  public function getClipsByProgramId($programId, $cursor = '') {
    $uri = '/v0/programs/' . $programId . '/clips';
    $params = [
      'programId' => $programId,
      'cursor' => $cursor,
      'pageSize' => '100',
    ];
    return $this->getApi($uri, $params);
  }

  /**
   * Get all migrated program ids.
   */
  public function getMigratedProgramIds() {
    $result = $this->database->select('taxonomy_term_data', 'td')
      ->fields('td', ['uuid'])
      ->condition('vid', 'omnystudio_programs', '=')
      ->execute();
    foreach ($result as $row) {
      $uuids[] = $row->uuid;
    }
    return $uuids;
  }

  /**
   * Retrieve a clip directly by Id without needing to specify a Program Id.
   */
  public function getClipsById($id) {
    $uri = '/v0/clips/' . $id;
    $params = [];
    return $this->getApi($uri, $params);
  }

  /**
   * Retrieve a clip using its Id and Program Id.
   */
  public function getClipsByProgramIdAndId($programId, $id) {
    $uri = '/v0/programs/' . $programId . '/clips/' . $id;
    $params = [];
    return $this->getApi($uri, $params);
  }

  /**
   * Update program artwork.
   */
  public function updateProgramArtwork($id, $artworkUrl) {
    $uri = '/v0/programs/' . $id . '/artwork';
    $params = [
      'body' => $artworkUrl,
      'headers' => ['Content-Type' => 'image/jpeg'],
    ];
    return $this->postApi($uri, $params);
  }

  /**
   * Update program details.
   */
  public function updateProgramDetails($id, $updateRequest) {
    $uri = '/v0/programs/' . $id;
    $params = [
      'json' => $updateRequest,
    ];
    return $this->postApi($uri, $params);
  }

  /**
   * Update playlist artwork.
   */
  public function updatePlaylistArtwork($id, $artworkUrl) {
    $uri = '/v0/playlists/' . $id . '/artwork';
    $params = [
      'body' => $artworkUrl,
      'headers' => ['Content-Type' => 'image/jpeg'],
    ];
    return $this->putApi($uri, $params);
  }

  /**
   * Update playlist details.
   */
  public function updatePlaylistDetails($id, $updateRequest) {
    $uri = '/v0/playlists/' . $id;
    $params = [
      'json' => $updateRequest,
    ];
    return $this->postApi($uri, $params);
  }

  /**
   * Update clip artwork.
   */
  public function updateClipArtwork($programId, $id, $artworkUrl) {
    $uri = '/v0/programs/' . $programId . '/clips/' . $id . '/artwork';
    $params = [
      'body' => $artworkUrl,
      'headers' => ['Content-Type' => 'image/jpeg'],
    ];
    return $this->putApi($uri, $params);
  }

  /**
   * Update clip details.
   */
  public function updateClipDetails($id, $updateRequest) {
    $uri = '/v0/clips/' . $id;
    $params = [
      'json' => $updateRequest,
    ];
    return $this->postApi($uri, $params);
  }

  /**
   * Create a new clip.
   */
  public function uploadClipByProgramId($programId, $createRequest) {
    $uri = '/v0/programs/' . $programId . '/clips';
    $params = [
      'json' => $createRequest,
    ];
    return $this->postApi($uri, $params);
  }

  /**
   * Upload an audio to an existing clip.
   */
  public function uploadClipAudio($programId, $clipId, $audioUrl) {
    $uri = '/v0/programs/' . $programId . '/clips/' . $clipId . '/audio';
    $params = [
      'query' => "audioUrl=". $audioUrl,
    ];
    return $this->putApi($uri, $params);
  }

}
