<?php

namespace Drupal\omnystudio\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class OmnyStudioSettingsForm.
 */
class OmnyStudioSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'omnystudio.settings';

  /**
   * Drupal\omnystudio\OmnyStudioApiInterface definition.
   *
   * @var \Drupal\omnystudio\OmnyStudioApiInterface
   */
  protected $omnystudioApi;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->omnystudioApi = $container->get('omnystudio.api');
    $instance->configFactory = $container->get('config.factory');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'omnystudio_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);
    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#description' => $this->t('Omnystudio api key'),
      '#maxlength' => 128,
      '#size' => 60,
      '#default_value' => $config->get('api_key'),
    ];
    $form['page_size'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Page Size'),
      '#description' => $this->t('Page size of the api request'),
      '#maxlength' => 64,
      '#size' => 60,
      '#default_value' => $config->get('page_size'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $api_key = $form_state->getValue('api_key');
    if (!$this->omnystudioApi->validCredentials($api_key)) {
      $form_state->setErrorByName('api_key', $this->t('The API Key is not valid.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->configFactory->getEditable(static::SETTINGS)
      // Set the submitted configuration setting.
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('page_size', $form_state->getValue('page_size'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
