<?php

namespace Drupal\omnystudio\Plugin\migrate_plus\data_fetcher;

use Drupal\Core\State\StateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate_plus\DataFetcherPluginBase;
use Drupal\omnystudio\OmnyStudioApiInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Retrieve data over from Omnystudio API.
 *
 * Example:
 *
 * @code
 * source:
 *   plugin: url
 *   data_fetcher_plugin: omnystudio_clip_http
 * @endcode
 *
 * @DataFetcher(
 *   id = "omnystudio_clip_http",
 *   title = @Translation("Omny Studio Data Fetcher")
 * )
 */
class OmnyStudioClipHttp extends DataFetcherPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The api manager interface.
   *
   * @var \Drupal\omnystudio\OmnyStudioApiInterface
   */
  protected $apiManager;

  /**
   * The cron manager.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration,
  $plugin_id,
  $plugin_definition,
  OmnyStudioApiInterface $api_manager,
  StateInterface $state,
  ConfigFactoryInterface $configFactory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->apiManager = $api_manager;
    $this->state = $state;
    $this->config = $configFactory->get('omnystudio.settings');
    $this->pageSize = $this->config->get('page_size');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('omnystudio.api'),
      $container->get('state'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setRequestHeaders(array $headers) {
  }

  /**
   * {@inheritdoc}
   */
  public function getRequestHeaders() {
  }

  /**
   * {@inheritdoc}
   */
  public function getResponse($url) {
    $last_cursor = $this->state->get('omnystudio_clip.cursor', '');
    // $last_cursor = '';
    $response = $this->apiManager->getClipsChanges($last_cursor);
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function getResponseContent($url) {
    return $this->getResponse($url);
  }

}
