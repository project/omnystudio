<?php

namespace Drupal\omnystudio\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * Alter the status.
 *
 * @MigrateProcessPlugin(
 *   id = "alter_path",
 * )
 */
class AlterPath extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (!$value) {
      return NULL;
    }
    // We cannot use guzzle here as it throws an exception and we want migration
    // to continue with skipping the entire row.
    // Use get_headers() function.
    $headers = @get_headers($value);

    // Use condition to check the existence of URL.
    if ($headers && strpos($headers[0], '404')) {
      return NULL;
    }
    else {
      // Return path without query params.
      $path = explode('?', $value);
      return $path[0];
    }

  }

}
