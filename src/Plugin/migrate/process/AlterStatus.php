<?php

namespace Drupal\omnystudio\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * Alter the status.
 *
 * @MigrateProcessPlugin(
 *   id = "alter_status",
 * )
 */
class AlterStatus extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    // Clips: PublishState->Published
    // Programs: Hidden->FALSE
    // Playlists: Visibility->Public
    if ($value === 'Published' || $value == FALSE || $value === 'Public') {
      return 1;
    }
    else {
      return 0;
    }
  }

}
