<?php

namespace Drupal\omnystudio;

/**
 * Interface OmnyStudioApiInterface.
 */
interface OmnyStudioApiInterface {

  /**
   * Get Palysists in a Network.
   *
   * @return array
   *   The list of playlists.
   */
  public function getNetworks();

}
