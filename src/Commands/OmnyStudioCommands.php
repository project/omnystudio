<?php

namespace Drupal\omnystudio\Commands;

use Drupal\Core\State\StateInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Plugin\MigrationPluginManagerInterface;
use Drush\Commands\DrushCommands;
use Drupal\migrate\MigrateExecutable;
use Drupal\migrate\MigrateMessage;
use Drupal\omnystudio\OmnyStudioApiInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Drush command for omnystudio.
 */
class OmnyStudioCommands extends DrushCommands {

  /**
   * The migration object.
   *
   * @var \Drupal\migrate\Plugin\MigrationPluginManagerInterface
   */
  protected $migrationManager;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The omnystudio program service.
   *
   * @var \Drupal\omnystudio\OmnyStudioApiInterface
   */
  protected $apiManager;

  /**
   * The Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  private $loggerChannelFactory;

  /**
   * OmnyStudioCommands constructor.
   *
   * @param \Drupal\migrate\Plugin\MigrationPluginManagerInterface $migration
   *   Migration plugin manager interface.
   * @param \Drupal\Core\State\StateInterface $state
   *   Interface for state system.
   * @param \Drupal\omnystudio\OmnyStudioApiInterface $apiManager
   *   Omnystudio program service.
   */
  public function __construct(MigrationPluginManagerInterface $migration, StateInterface $state, OmnyStudioApiInterface $apiManager, LoggerChannelFactoryInterface $loggerChannelFactory) {
    parent::__construct();

    $this->migrationManager = $migration;
    $this->state = $state;
    $this->apiManager = $apiManager;
    $this->loggerChannelFactory = $loggerChannelFactory;
  }

  /**
   * Imports videos from omnystudio and creates omnystudio media items.
   *
   * @option option-name
   *   Description
   * @usage omnystudio:sync
   *   Syncs the omnystudio videos
   *
   * @command omnystudio:sync
   * @aliases omnystudio-sync
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\migrate\MigrateException
   */
  public function sync() {
    /** @var \Drupal\migrate\Plugin\MigrationInterface $migration_network */
    $migration_network = $this->migrationManager->createInstance('omnystudio_network');
    /** @var \Drupal\migrate\Plugin\MigrationInterface $migration_program_image */
    $migration_program_image = $this->migrationManager->createInstance('omnystudio_program_image');
    /** @var \Drupal\migrate\Plugin\MigrationInterface $migration_program_media */
    $migration_program_media = $this->migrationManager->createInstance('omnystudio_program_media');
    /** @var \Drupal\migrate\Plugin\MigrationInterface $migration_program */
    $migration_program = $this->migrationManager->createInstance('omnystudio_program');
    /** @var \Drupal\migrate\Plugin\MigrationInterface $migration_playlist_image */
    $migration_playlist_image = $this->migrationManager->createInstance('omnystudio_playlist_image');
    /** @var \Drupal\migrate\Plugin\MigrationInterface $migration_playlist_media */
    $migration_playlist_media = $this->migrationManager->createInstance('omnystudio_playlist_media');
    /** @var \Drupal\migrate\Plugin\MigrationInterface $migration_playlist */
    $migration_playlist = $this->migrationManager->createInstance('omnystudio_playlist');
    /** @var \Drupal\migrate\Plugin\MigrationInterface $omnystudio_clip_image */
    $migration_clip_image = $this->migrationManager->createInstance('omnystudio_clip_image');
    /** @var \Drupal\migrate\Plugin\MigrationInterface $omnystudio_clip_media */
    $migration_clip_media = $this->migrationManager->createInstance('omnystudio_clip_media');
    /** @var \Drupal\migrate\Plugin\MigrationInterface $migration_clip */
    $migration_clip = $this->migrationManager->createInstance('omnystudio_clip');

    // Migrate omnystudio network.
    $networks = $this->migrate($migration_network);
    if ($networks && $networks == MigrationInterface::RESULT_COMPLETED) {
      $this->loggerChannelFactory->get('omnystudio')->info('Networks have been migrated & procedding to programs migration');
    }
    else {
      $this->loggerChannelFactory->get('omnystudio')->error('Networks could not be migrated.');
    }

    // Migrate omnnystudio program.
    $programs_image = $this->migrate($migration_program_image);
    if ($programs_image && $programs_image == MigrationInterface::RESULT_COMPLETED) {
      $this->loggerChannelFactory->get('omnystudio')->info('Programs Images have been migrated & procedding to programs media migration');
    }
    else {
      \Drupal::logger('omnystudio')->error('Programs Images could not be migrated.');
    }
    $programs_media = $this->migrate($migration_program_media);
    if ($programs_media && $programs_media == MigrationInterface::RESULT_COMPLETED) {
      $this->loggerChannelFactory->get('omnystudio')->info('Programs Media have been migrated & procedding to programs migration');
    }
    else {
      \Drupal::logger('omnystudio')->error('Programs Media could not be migrated.');
    }
    $programs = $this->migrate($migration_program);
    if ($programs && $programs == MigrationInterface::RESULT_COMPLETED) {
      // Setting the last cursor for programs.
      $last_cursor_program = $this->state->get('omnystudio_program.cursor', '');
      $response_program = $this->apiManager->getProgramChanges($last_cursor_program);
      $response_json_program = json_decode($response_program);
      if ($response_json_program) {
        $last_cursor_program = $response_json_program->Cursor;
      }
      $this->state->set('omnystudio_program.cursor', $last_cursor_program);
      $this->loggerChannelFactory->get('omnystudio')->info('Programs have been migrated & procedding to playlists migration');
      if (!empty($response_json_program->Deleted)) {
        omnystudio_unpublish_entities('taxonomy_term', $response_json_program->Deleted);
      }
    }
    else {
      \Drupal::logger('omnystudio')->error('Programs could not be migrated.');
    }

    // Migrate omnnystudio playlist.
    $playlists_image = $this->migrate($migration_playlist_image);
    if ($playlist_image && $playlist_image == MigrationInterface::RESULT_COMPLETED) {
      $this->loggerChannelFactory->get('omnystudio')->info('Playlist Images have been migrated & procedding to playlists media migration');
    }
    else {
      \Drupal::logger('omnystudio')->error('Playlists Images could not be migrated.');
    }
    $playlists_media = $this->migrate($migration_playlist_media);
    if ($playlists_media && $playlists_media == MigrationInterface::RESULT_COMPLETED) {
      $this->loggerChannelFactory->get('omnystudio')->info('Playlists Media have been migrated & procedding to playlists migration');
    }
    else {
      \Drupal::logger('omnystudio')->error('Playlists Media could not be migrated.');
    }
    $playlists = $this->migrate($migration_playlist);
    if ($playlists && $playlists == MigrationInterface::RESULT_COMPLETED) {
      // Setting the last cursor for playlists.
      $last_cursor_playlist = $this->state->get('omnystudio_playlist.cursor', '');
      $response_playlist = $this->apiManager->getPlaylistsChanges($last_cursor_playlist);
      $response_json_playlist = json_decode($response_playlist);
      if ($response_json_playlist) {
        $last_cursor_playlist = $response_json_playlist->Cursor;
      }
      $this->state->set('omnystudio_playlist.cursor', $last_cursor_playlist);
      $this->loggerChannelFactory->get('omnystudio')->info('Playlists have been migrated & procedding to clips migration');
      if (!empty($response_json_playlist->Deleted)) {
        omnystudio_unpublish_entities('taxonomy_term', $response_json_playlist->Deleted);
      }
    }
    else {
      \Drupal::logger('omnystudio')->error('Playlists could not be migrated.');
    }

    // Migate omnystudio clips.
    $clips_image = $this->migrate($migration_clip_image);
    if ($clips_image && $clips_image == MigrationInterface::RESULT_COMPLETED) {
      $this->loggerChannelFactory->get('omnystudio')->info('Clips Images have been migrated & procedding to clips media migration');
    }
    else {
      \Drupal::logger('omnystudio')->error('Clips Images could not be migrated.');
    }
    $clips_media = $this->migrate($migration_clip_media);
    if ($clips_media && $clips_media == MigrationInterface::RESULT_COMPLETED) {
      $this->loggerChannelFactory->get('omnystudio')->info('Clips Media have been migrated & procedding to clips migration');
    }
    else {
      \Drupal::logger('omnystudio')->error('Clips Media could not be migrated.');
    }
    $clips = $this->migrate($migration_clip);
    if ($clips && $clips == MigrationInterface::RESULT_COMPLETED) {
      // Setting the last cursor for clips.
      $last_cursor_clip = $this->state->get('omnystudio_clip.cursor', '');
      $response_clip = $this->apiManager->getClipsChanges($last_cursor_clip);
      $response_json_clip = json_decode($response_clip);
      if ($response_json_clip) {
        $last_cursor_clip = $response_json_clip->Cursor;
      }
      $this->state->set('omnystudio_clip.cursor', $last_cursor_clip);
      $this->loggerChannelFactory->get('omnystudio')->info('Clips have been migrated.');
      if (!empty($response_json_clip->Deleted)) {
        omnystudio_unpublish_entities('media', $response_json_clip->Deleted);
      }
    }
    else {
      $this->loggerChannelFactory->get('omnystudio')->error('Clips could not be migrated.');
    }

  }

  /**
   * Migrate the omnystudio videos and content.
   *
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   Migration plugin manager interface.
   *
   * @return int|null
   *   Returns migration status.
   */
  protected function migrate(MigrationInterface $migration) {
    if ($migration->getStatus() !== MigrationInterface::STATUS_IMPORTING) {
      $migration->setStatus(MigrationInterface::STATUS_IDLE);
      $migration->getIdMap()->prepareUpdate();

      try {
        $executable = new MigrateExecutable($migration, new MigrateMessage());
        return $executable->import();
      }
      catch (\Exception $exception) {
        $migration->setStatus(MigrationInterface::STATUS_IDLE);
        return NULL;
      }
    }
  }

}
