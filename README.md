Omnystudio
-----------
Omnystudio[https://omnystudio.com/] is an audio management solution for podcasts
and radio stations. This module is mainly a migration wrapper which lets you 
import data in Media(Audio) using the omnystudio management API 
[https://api.omnystudio.com/api-docs/index]

Install
--------
- Download and install this module the usual way.
- API Key and page size (max limit of no of items returned from API response)
can be configured at admin/config/omnystudio/omnystudiosettings.

Usage
------
- Four migrations are created under 'omnystudio' group which are dependent on 
each other and respective Vocabularies are being referenced to created content 
connection. Network->Programs->Playlists->Clips.
- Except Clips, everything else is a vocabulary. and clips are nothing but
'audio' media type.
- You can either manually import the data using migration UI or use the Drush
command which also takes care of the last imported data 'cursor'
- Module doesn't fetch each and every field that API provides but whateve
was most essential.
- It also syncs back few fields to omnystudio such as title, visibility,
category etc More info can be found in README.

Migrations (Source - JSON, Group - Omnystudio)
-------------
-	omnystudio_network
	Fetch all networks and stores them under vocab 'omnystudio networks'.
-	omnystudio_program (Depends on network)
	Fetch all programs and stores them under vocab 'omnystudio programs'.
	Field values that are synced back:
	- Name
	- Description
	- Artwork
	- Slug
	- Publisher
-	omnystudio_playlist (Depends on programs)
	Fetch all playlists and stores them under vocab 'omnystudio programs'.
	Field values that are synced back:
	- Name
	- Description
	- Artwork
	- Visibility
	- Category
-	omnystudio_clip (Depends on playlists)
	Fetch all clips and stores them under media type 'audio'
	Field values that are synced back:
	- Name
	- Description
	- PlaylistIds
	- ContentRating
	- Season
	- Episode
	- EpisodeType


Drush Command
--------------
omnystudio:sync - Sync networks, programs, playlists and clips. It stores last
cursor position so data with same id can be updated/deleted and only new is migrated


Current Module Status
----------------------
Tested with 8.8.x and further. Should work with any version of drupal 8.

